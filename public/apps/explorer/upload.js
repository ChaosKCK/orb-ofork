Dropzone.options['dropzone'] = {
	init: function() {
		this.on('error', function(file, data) {
			if (data.substr(0, 5) == '<?xml') {
				var error = parseInt($(data).find('error').text());
				switch (error) {
					case 400:
						alert('Error uploading file.');
						break;
					case 403:
						alert('The maximum of your storage capacity has been reached.');
						break;
					case 404:
						alert('Error uploading file to the specified destination.');
						break;
				}
			} else {
				alert(data);
			}
		});
	}
};
